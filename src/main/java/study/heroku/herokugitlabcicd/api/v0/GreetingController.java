package study.heroku.herokugitlabcicd.api.v0;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import study.heroku.herokugitlabcicd.api.v0.Greeting;

import java.util.concurrent.atomic.AtomicLong;

@RestController
@Slf4j
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(required=false, defaultValue="World") String name) {
        log.trace("==== in greeting ====");
        return Greeting.builder()
                .id(counter.incrementAndGet())
                .content(String.format(template, name))
                .build();
    }

}