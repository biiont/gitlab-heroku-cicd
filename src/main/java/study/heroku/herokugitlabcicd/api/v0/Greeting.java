package study.heroku.herokugitlabcicd.api.v0;

import lombok.*;

@Data
//@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class Greeting {

    private final long id;

    private final String content;

}
