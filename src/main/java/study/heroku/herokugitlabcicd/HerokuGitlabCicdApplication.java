package study.heroku.herokugitlabcicd;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class HerokuGitlabCicdApplication {

	public static void main(String[] args) throws IOException {
//		try (EmbeddedPostgres pg = EmbeddedPostgres.start()) {
			SpringApplication.run(HerokuGitlabCicdApplication.class, args);
//		}
	}

//	@Test
//	public void testEmbeddedPg() throws Exception
//	{
//		try (EmbeddedPostgres pg = EmbeddedPostgres.start();
//			 Connection c = pg.getPostgresDatabase().getConnection()) {
//			Statement s = c.createStatement();
//			ResultSet rs = s.executeQuery("SELECT 1");
//			assertTrue(rs.next());
//			assertEquals(1, rs.getInt(1));
//			assertFalse(rs.next());
//		}
//	}

//	@Test
//	public void testEmbeddedPgCreationWithNestedDataDirectory() throws Exception
//	{
//		try (EmbeddedPostgres pg = EmbeddedPostgres.builder()
//				.setDataDirectory(tf.newFolder("data-dir-parent") + "/data-dir")
//				.start()) {
//			// nothing to do
//		}
//	}

}
