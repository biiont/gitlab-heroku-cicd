package study.heroku.herokugitlabcicd;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import lombok.Data;
import org.slf4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
@Profile("dev")
public class DevelopmentConfiguration {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(DevelopmentConfiguration.class);

    @Bean
    @Primary
    public DataSource dataSource() throws IOException {
        EmbeddedPostgres pg = EmbeddedPostgres.start();
        return pg.getPostgresDatabase();
    }

    @Bean
    public ApplicationRunner initDataSource(DataSource ds) {
        return new ApplicationRunner() {
            @Override
            public void run(ApplicationArguments args) throws Exception {
                log.trace("Going to init dev DB...");
            }
        };
    }

}