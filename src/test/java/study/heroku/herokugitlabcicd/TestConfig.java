package study.heroku.herokugitlabcicd;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
public class TestConfig {

    @Bean
    @Primary
    public DataSource dataSource() throws IOException {
        EmbeddedPostgres pg = EmbeddedPostgres.start();
        return pg.getPostgresDatabase();
    }

}
