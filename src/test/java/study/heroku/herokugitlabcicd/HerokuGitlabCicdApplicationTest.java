package study.heroku.herokugitlabcicd;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@RunWith(SpringRunner.class)
public class HerokuGitlabCicdApplicationTest {

	@Test
	public void contextLoads() {
	}

}
